<?php

/**
 * Implements of hook_views_data().
 */
function dc_views_cart_products_filter_views_data() {
  return array(
    'commerce_product' => array(
      'cart_products' => array(
        'group' => t('Commerce Product'),
        'title' => t('In Cart'),
        'help' => t('Product in Cart'),
        'filter' => array(
          'handler' => 'dc_views_cart_products_filter_handler_filter_cart_products',
          'label' => t('In Cart'),
          'type' => 'yes-no',
        ),
      ),
    ),
  );
}
