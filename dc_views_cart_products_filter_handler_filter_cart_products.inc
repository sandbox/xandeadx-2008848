<?php

class dc_views_cart_products_filter_handler_filter_cart_products extends views_handler_filter_boolean_operator {
  function query() {
    $this->ensure_my_table();
    
    $product_ids = dc_views_cart_products_filter_get_cart_product_ids();
    if ($product_ids) {
      $operator = $this->value ? 'IN' : 'NOT IN';
      $this->query->add_where($this->options['group'], $this->table_alias . '.product_id', $product_ids, $operator);
    }
    elseif ($this->value) {
      $this->query->add_where($this->options['group'], $this->table_alias . '.product_id', '');
    }
  }
}
